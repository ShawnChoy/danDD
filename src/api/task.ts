import request from '@/utils/request'
import { commonError } from '@/utils/error'

// 新增任务
export const addTask = (data: any) => {
  return request({
    url: '/task',
    method: 'post',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}

// 任务详情
export const taskDetail = (params: string) => {
  return request({
    url: '/task/' + params,
    method: 'get'
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 任务列表
export const taskList = (params: any) => {
  return request({
    url: '/task',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 更新任务状态
export const taskStatus = (params: any) => {
  return request({
    url: '/task/status',
    method: 'put',
    data: params
  })
}
// 订单列表
export const order = (params: any) => {
  return request({
    url: '/order',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 订单列表导出
export const exportOrder = (params: any) => {
  return request({
    url: '/order',
    method: 'get',
    params
  }).then(res => {
    return res
  })
}
