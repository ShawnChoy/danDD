import request from '@/utils/request'
import { commonError } from '@/utils/error'

// 商家信息
export const getUser = (params: any) => {
  return request({
    url: '/user',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}

// 获取店铺列表
export const getShop = (params: any) => {
  return request({
    url: '/shop',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 新增店铺
export const addShop = (params: any) => {
  return request({
    url: '/shop',
    method: 'post',
    data: params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 更新店铺
export const editShop = (params: any) => {
  return request({
    url: '/shop',
    method: 'put',
    data: params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 获取商家列表
export const userList = (params: any) => {
  return request({
    url: '/user/list',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
