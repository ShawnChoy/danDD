import request from '@/utils/request'
import { commonError } from '@/utils/error'

// 审核列表
export const auditList = (params: any) => {
  return request({
    url: '/audit',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 审核
export const handleAudit = (data: any) => {
  return request({
    url: '/audit',
    method: 'put',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
