import request from '@/utils/request'
import { commonError } from '@/utils/error'

// 商家信息
export const getUser = (params: any) => {
  return request({
    url: '/user',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 登录
export const login = (data: any) => {
  return request({
    url: '/login',
    method: 'post',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 登出
export const logout = () => {
  return request({
    url: '/logout',
    method: 'put'
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 注册
export const register = (data: any) => {
  return request({
    url: '/register',
    method: 'post',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 发送短信
export const sendCode = (data: any) => {
  return request({
    url: '/sms_valid',
    method: 'post',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 修改密码
export const changePsd = (data: any) => {
  return request({
    url: '/password',
    method: 'put',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 重置密码
export const resetPass = (data: any) => {
  return request({
    url: '/user/password',
    method: 'put',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 获取银行卡列表
export const getUserBank = (params: any) => {
  return request({
    url: '/user_bank',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 添加银行卡
export const postUserBank = (data: any) => {
  return request({
    url: '/user_bank',
    method: 'post',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 编辑银行卡
export const putUserBank = (data: any) => {
  return request({
    url: '/user_bank',
    method: 'put',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 删除银行卡
export const delUserBank = (data: any) => {
  return request({
    url: '/user_bank',
    method: 'delete',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 充值
export const recharge = (data: any) => {
  return request({
    url: '/recharge',
    method: 'post',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 提现
export const withdraw = (data: any) => {
  return request({
    url: '/withdraw',
    method: 'post',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 充值提现列表
export const accountInOut = (params: any) => {
  return request({
    url: '/account_in_out',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 充值提现列表
export const accountInOutStatus = (data: any) => {
  return request({
    url: '/account_in_out/status',
    method: 'put',
    data
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 资金明细
export const accountLog = (params: any) => {
  return request({
    url: '/account_log',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
