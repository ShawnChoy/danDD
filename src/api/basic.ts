import request from '@/utils/request'
import { commonError } from '@/utils/error'

// 获取类别
export const category = (params: any) => {
  return request({
    url: '/category',
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
// 获取配置
export const config = (params: any) => {
  let url = '/config'
  console.log(typeof params)
  if (typeof params === 'string') {
    url += `/${params}`
  } else {
    url = '/config'
  }
  console.log('url: ---->', url)
  return request({
    url,
    method: 'get',
    params
  }).then(res => {
    if (commonError(res)) return res
  })
}
