/*
 * @Date: 2021-04-12 22:10:32
 * @Author: cxa
 * @LastEditors: cxa
 * @LastEditTime: 2021-04-15 17:43:56
 * @FilePath: /danDD/src/utils/request.ts
 * @Description:
 */
import axios from 'axios'
import { Message } from 'element-ui'
import { UserModule } from '@/store/modules/user'
const service = axios.create({
  baseURL: '/api', // url = base url + request url
  timeout: 50000
  // withCredentials: true // send cookies when cross-domain requests
})

// Request interceptors
service.interceptors.request.use(
  (config) => {
    // Add X-Access-Token header to every request, you can add other custom headers here
    if (UserModule.token) {
      config.headers.Authorization = `Bearer ${UserModule.token}`
    }
    return config
  },
  (error) => {
    Promise.reject(error)
  }
)

// Response interceptors
service.interceptors.response.use(
  (response) => {
    // Some example codes here:
    // code == 0: success
    // code == 50001: invalid access token
    // code == 50002: already login in other place
    // code == 50003: access token expired
    // code == 50004: invalid user (user not exist)
    // code == 50005: username or password is incorrect
    // You can change this part for your own usage.
    const res = response.data
    if (response.status === 200) {
      if (response.headers['content-disposition']) {
        const filename = response.headers['content-disposition']
        window.localStorage.setItem('filename', filename.split(';')[1] && filename.split(';')[1].split('=')[1])
      }
      if (res && res.code && res.code !== 0) {
        Message({
          message: res.msg || 'Error',
          type: 'error',
          duration: 5 * 1000
        })
        if (res.code === 20002) {
          UserModule.ResetToken()
          location.reload()
        }
        return Promise.reject(new Error(res.msg || 'Error'))
      } else {
        return response.data
      }
    }
  },
  (error) => {
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
