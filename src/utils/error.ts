
import { Message } from 'element-ui'
// import store from '@/store'

// 通常错误码
/**
 *      0: OK
 *  10001: 内部服务错误!
    10002: api不存在!
    10003: 用户未登录!
    10004: 用户已登录!
    10005: 参数验证失败!
    10006: 没有权限!
    10007: 账号不存在!
    10008: 数据不存在!
    10009: 类型转换失败!
*/
export function commonError(res: any) {
  const { code, msg } = res
  if (code !== 0) {
    Message({
      showClose: true,
      message: msg,
      type: 'error'
    })
    return false
  }

  // // 10001: 内部服务错误
  // if (code === 10001) {
  //   Message({
  //     showClose: true,
  //     // message: '内部服务器错误，请联系研发人员',
  //     type: 'error'
  //   })
  //   return false
  // }

  // // 10002: api不存在
  // if (code === 10002) {
  //   Message({
  //     showClose: true,
  //     message: 'api不存在，请联系研发人员',
  //     type: 'error'
  //   })
  //   return false
  // }

  // // 10003: 用户未登录
  if (code === 20002) {
    Message({
      showClose: true,
      message: '请重新登录',
      type: 'error'
    })
    location.reload()
    return false
  }

  // // 10004: 用户已登录
  // if (code === 10004) {
  //   Message({
  //     showClose: true,
  //     message: '用户已登录',
  //     type: 'warning'
  //   })
  //   return false
  // }

  // // 10005：参数验证失败
  // if (code === 10005) {
  //   Message({
  //     showClose: true,
  //     message: '参数验证失败, 请联系研发人员',
  //     type: 'error'
  //   })
  //   return false
  // }

  // // 10006: 没有权限
  // if (code === 10006) {
  //   Message({
  //     showClose: true,
  //     message: '没有权限',
  //     type: 'warning'
  //   })
  //   return false
  // }

  // // 10007: 账号不存在
  // if (code === 10007) {
  //   Message({
  //     showClose: true,
  //     message: '账号不存在',
  //     type: 'error'
  //   })
  //   return false
  // }

  // // 10008: 数据不存在!
  // if (code === 10008) {
  //   Message({
  //     showClose: true,
  //     message: '数据不存在',
  //     type: 'error'
  //   })
  //   return false
  // }

  // // 10009: 类型转换失败!
  // if (code === 10009) {
  //   Message({
  //     showClose: true,
  //     message: '类型转换失败，请联系研发人员',
  //     type: 'error'
  //   })
  //   return false
  // }
  return true
}
