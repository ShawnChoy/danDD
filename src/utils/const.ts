export const Platforms = [
  { label: '淘宝', key: 'TB' },
  { label: '京东', key: 'JD' },
  { label: '拼多多', key: 'PDD' },
  { label: '抖音', key: 'DY' },
  { label: '快手', key: 'KS' }
]

export const FilePrefix = 'http://file.caicai.site/'

export const EvalTypes = {
  normal: '普通任务',
  words: '指定文字好评任务',
  img: '指定图片好评任务',
  video: '指定视频好评任务'
}
