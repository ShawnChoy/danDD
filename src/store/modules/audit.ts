import { VuexModule, Module, Action, getModule } from 'vuex-module-decorators'
import { auditList, handleAudit } from '@/api/audit'
import store from '@/store'

export interface IAuditState {
  cate: string
}

@Module({ dynamic: true, store, name: 'audit' })
class Audit extends VuexModule implements IAuditState {
  public cate = ''
  // 审核列表
  @Action
  public async AuditList(params: any) {
    const res: any = await auditList(params)
    if (res.code === 0) {
      return res.data
    }
  }

  // 审核
  @Action
  public async HandleAudit(params: any) {
    const res: any = await handleAudit(params)
    if (res.code === 0) {
      return res.data
    }
  }
}

export const AuditModule = getModule(Audit)
