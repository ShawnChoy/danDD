import { VuexModule, Module, Action, getModule } from 'vuex-module-decorators'
import { addTask, taskDetail, taskList, taskStatus, order, exportOrder } from '@/api/task'
import store from '@/store'

export interface ITaskState {
  cate: string
}

@Module({ dynamic: true, store, name: 'business' })
class Task extends VuexModule implements ITaskState {
  public cate = ''
  // 添加任务
  @Action
  public async AddTask(params: any) {
    const res: any = await addTask(params)
    if (res.code === 0) {
      return res.data
    }
  }

  // 任务详情
  @Action
  public async TaskDetail(params: any) {
    const res: any = await taskDetail(params)
    if (res.code === 0) {
      return res.data
    }
  }

  // 任务列表
  @Action
  public async TaskList(params: any) {
    const res: any = await taskList(params)
    if (res.code === 0) {
      return res.data
    }
  }

  // 更新任务状态
  @Action
  public async TaskStatus(params: any) {
    const res: any = await taskStatus(params)
    if (res.code === 0) {
      return res
    }
  }

  // 订单列表
  @Action
  public async Order(params: any) {
    const res: any = await order(params)
    if (res.code === 0) {
      return res.data
    }
  }

  // 订单列表
  @Action
  public async ExportOrder(params: any) {
    const res: any = await exportOrder(params)
    console.log(res)
    return res
  }
}

export const TaskModule = getModule(Task)
