import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators'
import {
  login,
  logout,
  register,
  getUser,
  changePsd,
  getUserBank,
  postUserBank,
  putUserBank,
  delUserBank,
  recharge,
  withdraw,
  sendCode,
  accountInOut,
  accountInOutStatus,
  accountLog,
  resetPass
} from '@/api/users'
import { getToken, setToken, removeToken } from '@/utils/cookies'
import router, { resetRouter } from '@/router'
import { PermissionModule } from './permission'
import { TagsViewModule } from './tags-view'
import store from '@/store'

export interface IUserState {
  token: string
  name: string
  avatar: string
  introduction: string
  roles: string[]
  email: string
  userInfo: object
}

@Module({ dynamic: true, store, name: 'user' })
class User extends VuexModule implements IUserState {
  public token = getToken() || ''
  public name = ''
  public avatar = ''
  public introduction = ''
  public roles: string[] = []
  public email = ''
  public userInfo: any = {}

  @Mutation
  private SET_TOKEN(token: string) {
    this.token = token
  }

  @Mutation
  private SET_NAME(name: string) {
    this.name = name
  }

  @Mutation
  private SET_AVATAR(avatar: string) {
    this.avatar = avatar
  }

  @Mutation
  private SET_INTRODUCTION(introduction: string) {
    this.introduction = introduction
  }

  @Mutation
  private SET_ROLES(roles: string[]) {
    this.roles = roles
  }

  @Mutation
  private SET_EMAIL(email: string) {
    this.email = email
  }

  @Mutation
  private SET_USER(userInfo: Object) {
    this.userInfo = userInfo
  }

  @Action
  public async Login(userInfo: { mobile: string, password: string}) {
    const { mobile, password } = userInfo
    const res: any = await login({ mobile, password })
    if (res.code === 0) {
      const { token } = res.data
      setToken(token)
      this.SET_TOKEN(token)
      return res.data
    }
  }

  @Action
  public ResetToken() {
    removeToken()
    this.SET_TOKEN('')
    this.SET_ROLES([])
  }

  // 获取商家信息
  @Action({ rawError: true })
  public async GetUser() {
    const res: any = await getUser({})
    if (res.code === 0) {
      return res.data
    }
  }

  @Action
  public async GetUserInfo() {
    const res: any = await getUser({ /* Your params here */ })
    if (res.code === 0) {
      this.SET_ROLES([res.data.user.type])
      // this.SET_ROLES(['admin'])
      this.SET_USER({ ...res.data.user, amount: res.data.amount, frozen_amount: res.data.frozen_amount, shop_count: res.data.shop_count })
    }
  }

  @Action
  public async ChangeRoles(role: string) {
    console.log('role: ---> ', role)
    // Dynamically modify permissions
    const token = role + '-token'
    this.SET_TOKEN(token)
    setToken(token)
    await this.GetUserInfo()
    resetRouter()
    // Generate dynamic accessible routes based on roles
    PermissionModule.GenerateRoutes(this.roles)
    // Add generated routes
    router.addRoutes(PermissionModule.dynamicRoutes)
    // Reset visited views and cached views
    TagsViewModule.delAllViews()
  }

  // 登出
  @Action
  public async LogOut() {
    if (this.token === '') {
      throw Error('LogOut: token is undefined!')
    }
    await logout()
    removeToken()
    resetRouter()

    // Reset visited views and cached views
    TagsViewModule.delAllViews()
    this.SET_TOKEN('')
    this.SET_ROLES([])
  }

  // 注册
  @Action
  public async Register(userInfo: any) {
    const res:any = await register(userInfo)
    if (res.code === 0) {
      return res.data
    }
  }

  // 修改密码
  @Action
  public async ChangePsd(data: any) {
    const res:any = await changePsd(data)
    if (res.code === 0) {
      return res
    }
  }

  // 重置密码
  @Action
  public async ResetPass(data: any) {
    const res: any = await resetPass(data)
    if (res.code === 0) {
      return res
    }
  }

  // 发送验证码
  @Action
  public async SendCode(data: any) {
    const res:any = await sendCode(data)
    if (res.code === 0) {
      return res
    }
  }

  // 获取银行卡列表
  @Action
  public async GetUserBank(data: any) {
    const res: any = await getUserBank(data)
    if (res.code === 0) {
      return res.data
    }
  }

  // 添加银行卡
  @Action
  public async PostUserBank(data: any) {
    const res: any = await postUserBank(data)
    if (res.code === 0) {
      return res.data
    }
  }

  // 编辑银行卡
  @Action
  public async PutUserBank(data: any) {
    const res: any = await putUserBank(data)
    if (res.code === 0) {
      return res.data
    }
  }

  // 删除银行卡
  @Action
  public async DelUserBank(data: any) {
    const res: any = await delUserBank(data)
    if (res.code === 0) {
      return res.data
    }
  }

  // 充值
  @Action
  public async Recharge(data: any) {
    const res: any = await recharge(data)
    if (res.code === 0) {
      return res.data
    }
  }

  // 提现
  @Action
  public async Withdraw(data: any) {
    const res: any = await withdraw(data)
    if (res.code === 0) {
      return res
    }
  }

  // 充值提现列表
  @Action
  public async AccountInOut(data: any) {
    const res: any = await accountInOut(data)
    if (res.code === 0) {
      return res.data
    }
  }

  // 撤销充值提现
  @Action
  public async AccountInOutStatus(data:any) {
    const res: any = await accountInOutStatus(data)
    if (res.code === 0) {
      return res
    }
  }

  // 资金明细
  @Action
  public async AccountLog(data:any) {
    const res: any = await accountLog(data)
    if (res.code === 0) {
      return res.data
    }
  }
}

export const UserModule = getModule(User)
