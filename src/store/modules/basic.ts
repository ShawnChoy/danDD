import { VuexModule, Module, Action, getModule } from 'vuex-module-decorators'
import { category, config } from '@/api/basic'
import store from '@/store'

export interface IBasicState {
  cate: string
}

@Module({ dynamic: true, store, name: 'business' })
class Basic extends VuexModule implements IBasicState {
  public cate = ''
  // 获取类别
  @Action
  public async Category(params: any) {
    const res: any = await category(params)
    if (res.code === 0) {
      return res.data
    }
  }

  // 获取配置
  @Action
  public async GetConfig(params: any) {
    const res: any = await config(params)
    if (res.code === 0) {
      return res.data
    }
  }
}

export const BasicModule = getModule(Basic)
