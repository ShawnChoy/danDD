import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators'
import { addShop, getShop, getUser, editShop, userList } from '@/api/business'
import store from '@/store'

export interface IBusinessState {
  token: string
}

@Module({ dynamic: true, store, name: 'business' })
class Business extends VuexModule implements IBusinessState {
  public token = ''
  @Mutation
  private SET_TOKEN(token: string) {
    console.log(token)
  }

  // 获取商家信息
  @Action
  public async GetUser() {
    const res: any = await getUser({})
    return res.data
  }

  // 获取店铺列表
  @Action
  public async GetShop() {
    const res: any = await getShop({})
    if (res.code === 0) {
      return res.data
    }
  }

  // 添加店铺
  @Action
  public async AddShop(payload: object) {
    const res: any = await addShop(payload)
    if (res.code === 0) {
      return res.data
    }
  }

  // 更新店铺
  @Action
  public async EditShop(payload: any) {
    const res:any = await editShop(payload)
    if (res.code === 0) {
      return res.data
    }
  }

  // 获取商家列表
  @Action
  public async UserList(payload: any) {
    const res: any = await userList(payload)
    if (res.code === 0) {
      return res.data
    }
  }
}

export const BusinessModule = getModule(Business)
