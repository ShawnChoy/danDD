import Vue from 'vue'
import Vuex from 'vuex'
import { IAppState } from './modules/app'
import { IUserState } from './modules/user'
import { ITagsViewState } from './modules/tags-view'
import { IErrorLogState } from './modules/error-log'
import { IPermissionState } from './modules/permission'
import { ISettingsState } from './modules/settings'
import { IBusinessState } from './modules/business'
import { IBasicState } from './modules/basic'
import { ITaskState } from './modules/task'
import { IAuditState } from './modules/audit'

Vue.use(Vuex)

export interface IRootState {
  app: IAppState
  user: IUserState
  tagsView: ITagsViewState
  errorLog: IErrorLogState
  permission: IPermissionState
  settings: ISettingsState
  business: IBusinessState
  basic: IBasicState
  task: ITaskState
  audit: IAuditState
}

// Declare empty store first, dynamically register all modules later.
export default new Vuex.Store<IRootState>({})
