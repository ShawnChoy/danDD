import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

/* Layout */
import Layout from '@/layout/index.vue'

/* Router modules */
// import componentsRouter from './modules/components'
// import chartsRouter from './modules/charts'
// import tableRouter from './modules/table'
// import nestedRouter from './modules/nested'

Vue.use(VueRouter)

/*
  Note: sub-menu only appear when children.length>=1
  Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
*/

/*
  name:'router-name'             the name field is required when using <keep-alive>, it should also match its component's name property
                                 detail see : https://vuejs.org/v2/guide/components-dynamic-async.html#keep-alive-with-Dynamic-Components
  redirect:                      if set to 'noredirect', no redirect action will be trigger when clicking the breadcrumb
  meta: {
    roles: ['admin', 'shop']   will control the page roles (allow setting multiple roles)
    title: 'title'               the name showed in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon showed in the sidebar
    hidden: true                 if true, this route will not show in the sidebar (default is false)
    alwaysShow: true             if true, will always show the root menu (default is false)
                                 if false, hide the root menu when has less or equal than one children route
    breadcrumb: false            if false, the item will be hidden in breadcrumb (default is true)
    noCache: true                if true, the page will not be cached (default is false)
    affix: true                  if true, the tag will affix in the tags-view
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
*/

/**
  ConstantRoutes
  a base page that does not have permission requirements
  all roles can be accessed
*/
export const constantRoutes: RouteConfig[] = [
  {
    path: '/redirect',
    component: Layout,
    meta: { hidden: true },
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import(/* webpackChunkName: "redirect" */ '@/views/redirect/index.vue')
      }
    ]
  },
  {
    path: '/login', // 登录
    component: () => import('@/views/login/index.vue'),
    meta: { hidden: true }
  },
  {
    path: '/register', // 注册
    component: () => import('@/views/register/index.vue'),
    meta: { hidden: true }
  },
  {
    path: '/forget', // 注册
    component: () => import('@/views/forget/index.vue'),
    meta: { hidden: true }
  },
  {
    path: '/auth-redirect',
    component: () => import(/* webpackChunkName: "auth-redirect" */ '@/views/login/auth-redirect.vue'),
    meta: { hidden: true }
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "404" */ '@/views/error-page/404.vue'),
    meta: { hidden: true }
  },
  {
    path: '/401',
    component: () => import(/* webpackChunkName: "401" */ '@/views/error-page/401.vue'),
    meta: { hidden: true }
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/dashboard/index.vue'),
        name: 'Dashboard',
        meta: {
          title: 'dashboard',
          icon: 'dashboard',
          affix: true
        }
      }
    ]
  },
  {
    path: '/profile', // 个人中心
    component: Layout,
    redirect: '/profile/index',
    meta: { hidden: true },
    children: [
      {
        path: 'index',
        component: () => import(/* webpackChunkName: "profile" */ '@/views/profile/index.vue'),
        name: 'Profile',
        meta: {
          title: 'profile',
          icon: 'user',
          noCache: true
        }
      }
    ]
  }
  // {
  //   path: '/task',
  //   component: Layout,
  //   redirect: '/task/index',
  //   meta: { hidden: false, title: 'task', icon: 'list' },
  //   children: [
  //     {
  //       path: 'index/:id',
  //       component: () => import('@/views/task/index.vue'),
  //       name: 'Task',
  //       meta: {
  //         title: 'sendTask',
  //         icon: 'user',
  //         noCache: true
  //       }
  //     },
  //     {
  //       path: 'list',
  //       component: () => import('@/views/task/list.vue'),
  //       name: 'TaskList',
  //       meta: {
  //         title: 'taskList',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     },
  //     {
  //       path: 'detail',
  //       component: () => import('@/views/task/detail.vue'),
  //       name: 'TaskDetail',
  //       meta: {
  //         title: 'taskDetail',
  //         icon: 'list',
  //         noCache: true,
  //         hidden: true
  //       }
  //     },
  //     {
  //       path: 'order',
  //       component: () => import('@/views/task/order.vue'),
  //       name: 'OrderList',
  //       meta: {
  //         title: 'orderList',
  //         icon: 'list'
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/account',
  //   component: Layout,
  //   redirect: '/account/index',
  //   meta: { hidden: false, title: 'account', icon: 'user' },
  //   children: [
  //     {
  //       path: 'shop',
  //       component: () => import('@/views/account/shop.vue'),
  //       name: 'Account',
  //       meta: {
  //         title: 'shop',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     }, {
  //       path: 'businessList',
  //       component: () => import('@/views/account/businessList.vue'),
  //       name: 'Account',
  //       meta: {
  //         title: 'businessList',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/financial',
  //   component: Layout,
  //   redirect: '/financial/index',
  //   meta: { hidden: false, title: 'financial', icon: 'dashboard' },
  //   children: [
  //     {
  //       path: 'recharge',
  //       component: () => import('@/views/financial/recharge.vue'),
  //       name: 'Financial',
  //       meta: {
  //         title: 'recharge',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     }, {
  //       path: 'withdraw',
  //       component: () => import('@/views/financial/withdraw.vue'),
  //       name: 'Financial',
  //       meta: {
  //         title: 'withdraw',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     }, {
  //       path: 'bank',
  //       component: () => import('@/views/financial/bankList.vue'),
  //       name: 'Financial',
  //       meta: {
  //         title: 'bank',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     }, {
  //       path: 'details',
  //       component: () => import('@/views/financial/details.vue'),
  //       name: 'Financial',
  //       meta: {
  //         title: 'details',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/fees',
  //   component: Layout,
  //   redirect: '/fees/index',
  //   meta: { hidden: false, title: 'fees', icon: 'dashboard' },
  //   name: 'Fees',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/fees/index.vue'),
  //       name: 'Fees',
  //       meta: {
  //         title: 'fees',
  //         icon: 'list',
  //         noCache: true
  //       }
  //     }
  //   ]
  // }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
*/
export const asyncRoutes: RouteConfig[] = [
  {
    path: '/task',
    component: Layout,
    redirect: '/task/index',
    meta: { hidden: false, title: 'task', icon: 'list', roles: ['admin', 'shop', 'buyer'] },
    children: [
      {
        path: 'index/:id',
        component: () => import('@/views/task/index.vue'),
        name: 'Task',
        meta: {
          title: 'sendTask',
          icon: 'user',
          roles: ['admin', 'shop'],
          noCache: true
        }
      },
      {
        path: 'list',
        component: () => import('@/views/task/list.vue'),
        name: 'TaskList',
        meta: {
          title: 'taskList',
          icon: 'list',
          roles: ['admin', 'shop'],
          noCache: true
        }
      },
      {
        path: 'detail',
        component: () => import('@/views/task/detail.vue'),
        name: 'TaskDetail',
        meta: {
          title: 'taskDetail',
          icon: 'list',
          noCache: true,
          roles: ['admin', 'shop'],
          hidden: true
        }
      },
      {
        path: 'order',
        component: () => import('@/views/task/order.vue'),
        name: 'OrderList',
        meta: {
          title: 'orderList',
          roles: ['admin', 'shop', 'buyer'],
          icon: 'list'
        }
      // },
      // {
      //   path: 'refundOrder',
      //   component: () => import('@/views/task/refundOrder.vue'),
      //   name: 'RefundOrder',
      //   meta: {
      //     title: 'refundOrder',
      //     icon: 'list',
      //     noCache: true,
      //     roles: ['admin', 'shop']
      //   }
      }
    ]
  },
  {
    path: '/audit',
    component: Layout,
    redirect: '/audit/index',
    meta: {
      hidden: false,
      title: 'audit',
      roles: ['admin'],
      icon: 'list'
    },
    children: [
      {
        path: 'audit',
        component: () => import('@/views/audit/index.vue'),
        name: 'Audit',
        meta: {
          title: 'audit',
          icon: 'list',
          noCache: true,
          roles: ['admin']
        }
      }
    ]
  },
  {
    path: '/account',
    component: Layout,
    redirect: '/account/index',
    meta: {
      hidden: false,
      title: 'account',
      roles: ['admin', 'shop'],
      icon: 'user'
    },
    children: [
      {
        path: 'shop',
        component: () => import('@/views/account/shop.vue'),
        name: 'Account',
        meta: {
          title: 'shop',
          icon: 'list',
          roles: ['admin', 'shop'],
          noCache: true
        }
      }, {
        path: 'businessList',
        component: () => import('@/views/account/businessList.vue'),
        name: 'Account',
        meta: {
          title: 'businessList',
          icon: 'list',
          roles: ['admin'],
          noCache: true
        }
      }
    ]
  },
  {
    path: '/financial',
    component: Layout,
    redirect: '/financial/index',
    meta: { hidden: false, title: 'financial', icon: 'dashboard' },
    children: [
      {
        path: 'recharge',
        component: () => import('@/views/financial/recharge.vue'),
        name: 'Financial',
        meta: {
          title: 'recharge',
          icon: 'list',
          roles: ['admin', 'shop'],
          noCache: true
        }
      }, {
        path: 'withdraw',
        component: () => import('@/views/financial/withdraw.vue'),
        name: 'Financial',
        meta: {
          title: 'withdraw',
          icon: 'list',
          roles: ['admin', 'shop'],
          noCache: true
        }
      }, {
        path: 'bank',
        component: () => import('@/views/financial/bankList.vue'),
        name: 'Financial',
        meta: {
          title: 'bank',
          roles: ['admin', 'shop'],
          icon: 'list',
          noCache: true
        }
      }, {
        path: 'details',
        component: () => import('@/views/financial/details.vue'),
        name: 'Financial',
        meta: {
          title: 'details',
          icon: 'list',
          roles: ['admin', 'shop'],
          noCache: true
        }
      }
    ]
  },
  {
    path: '/fees',
    component: Layout,
    redirect: '/fees/index',
    meta: { hidden: false, title: 'fees', icon: 'dashboard' },
    name: 'Fees',
    children: [
      {
        path: 'index',
        component: () => import('@/views/fees/index.vue'),
        name: 'Fees',
        meta: {
          title: 'fees',
          icon: 'list',
          roles: ['admin', 'shop'],
          noCache: true
        }
      }
    ]
  },
  {
    path: '/permission',
    component: Layout,
    redirect: '/permission/directive',
    meta: {
      title: 'permission',
      icon: 'lock',
      roles: ['admin'], // you can set roles in root nav
      alwaysShow: true // will always show the root menu
    },
    children: [
      {
        path: 'page',
        component: () => import(/* webpackChunkName: "permission-page" */ '@/views/permission/page.vue'),
        name: 'PagePermission',
        meta: {
          title: 'pagePermission',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      },
      {
        path: 'directive',
        component: () => import(/* webpackChunkName: "permission-directive" */ '@/views/permission/directive.vue'),
        name: 'DirectivePermission',
        meta: {
          title: 'directivePermission'
          // if do not set roles, means: this page does not require permission
        }
      },
      {
        path: 'role',
        component: () => import(/* webpackChunkName: "permission-role" */ '@/views/permission/role.vue'),
        name: 'RolePermis',
        meta: {
          title: 'rolePermission',
          roles: ['admin']
        }
      }
    ]
  }
]

const createRouter = () => new VueRouter({
  // mode: 'history',  // Disabled due to Github Pages doesn't support this, enable this if you need.
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  base: process.env.BASE_URL,
  routes: [...constantRoutes, ...asyncRoutes]
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  (router as any).matcher = (newRouter as any).matcher // reset router
}

export default router
